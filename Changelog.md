0.1.75 Fix for target undefined.  
0.1.74 Fixed edge caase when using better rolls and attacks miss, but still rolled savaes.  
0.1.73 added spell cast consmption compatibility. That covers the known problems as far as  my limted testing is concerned. No support for DR/DI/DV to non-maigical-physical damage yet.  
0.1.72 First pass of dnd 0.95 compatibility  
0.1.71 reinstate better rolls support  
0.1.70 move weapon name out of hit string to make more space in hit chat card  
fixup call to notifyStatus to occur before reset.  
0.1.69 [Breaking] A bit. autoShiftClick now has 4 values none (the default), attack only, damage only, attack and damage to allow for finer grained workflow. You should set this config item after updating.  
0.1.68 Change the text for Damage Applied to Hit Points Updated  
add data to Hooks.callAll("MinorQolRollComplete", data)  
where data is:
```
actor: Actor5e {data: {…}, options: {…}, apps: {…}, compendium: null, items: Map(114), …}
attackRoll: Roll {data: {…}, terms: Array(5), _formula: "1d20 + 9 + 2", _dice: Array(0), results: Array(5), …}
attackTotal: 16
damageList: [{…}]
damageRoll: Roll {data: {…}, terms: Array(1), _formula: "1d8", _dice: Array(0), results: Array(1), …}
damageTotal: 4
failedSaves: Set(0) {}
hitTargets: Set(3) {Token, Token, Token}
isCritical: false
isFumble: false
isHit: true
item: Item5e {data: {…}, options: {…}, apps: {…}, compendium: null, labels: {…}}
saves: Set(0) {}
spellLevel: undefined
targets: Set(3) {Token, Token, Token}
token: Token {_events: i, _eventsCount: 2, tempDisplayObjectParent: null, transform: t, alpha: 1, …}
versatile: false
```
0.1.67 repackage push  
0.1.66 [BREAKING] If an item has a saving throw and auto check saving throws are disabled damage application will not happen.  
0.1.65 fix for better rolls with attack and saves where it was ignoring the save settings.  
0.1.64 [BREAKING] My enenmies' enemy is my friend mode for enemy/ally targeting.  
A change to how enemy/ally targeting works:  
Tokens with "friendly disposition tokens "enemy" means "hostilecreatures with an opposite disposition are targeted (i.e. for hostile tokens, "enemy" will target friendly disposition tokens, "ally" will target hostile disposition tokens, and vice versa). Any spells/effects enemies/allies will need to be checked.  
ally/enemy resolution now applies to all item rolls.  
Support for LMRTFY query mode (Player Roll Saves "LMRTFY + Query")  
0.1.63 fix for multi level token targeting (I hope)  
0.1.62 fix for betterNPCActor5eSheet  
fix for betterRolls templates and feats  
fix for resource consumption when not auto rolling damage  
0.1.61 Fixed edge case issue with firefox and DoTrapAttack macro.  
Parse flavor text for damage type - only used if there is not item damage type to use.  
0.1.60 Fixed two bad error messages  
Added support for downtime module (Don't override click settings)  
0.1.59 fix for max critical not working.  
updated to make sure all paths are relative  
added merged ko.json  
0.1.58 Added alternative critical damage.  
0.1.57 fixed display of to hit chat card despite the hide setting  
0.1.56 language packs merges  
0.1.55 added support for monster block  
0.1.54 foundryvtt 0.7.0 compatible.  
Added option "Auto Shift Click" for speed rolls. Defaults to true and means clicking to use the item/cast the spell and auto rolled damage will bypass the normal roll dialog. If false the normal dialog will be used.  
0.1.53 Fix for forceRollDamage being too aggressive.  
Clean up some whisper duplication  
Slight change to resource consumption for weapons to bring it into line with default dnd5e behaviour. Charges are only consumed if the resource consumption is specified.  
Changed damage immunities so that any effect that has the text "no damage on save" and has a saving throw will do no damage.  
v0.1.52 packaging snafu
v0.1.50/51 Fix for consumption
0.1.48 some bug fixes...
Fix for better rolls and healing damage.  
New config option to use token names instead of actor names almost everywhere  
Bugfix reset spellLevel after cast completes.  
Don't send whispers to GMs that are not logged on.  
v0.1.46  fix for better rolls and webm tokens  
fix for webm tokens and saving throws  
change settings so that choosing speed rolls does not require a reload just closing and opening character sheets  
v0.1.45 Changed name displayed in chat cards to be the token name, rather than the actor name.  
Support webm tokens being displayed in chat cards  
add versatile damage button to chat card display  
v0.1.44 Fix for healing  
spells when not using speed rolls (i.e. standard chat card) and auto applying damage.  
Fix for not auto rolling damage and spell scaling when not auto rolling damage but having minor-qol always display the chat card.  
Pass actual damage applied as damageTotal when exactly 1 token is targeted.  
v0.1.43 Finally fix localised damage types in better rolls processing so that immunities/resistances work.  
fix for better rolls damage type "None"  
fix for spell scaling when not autorolling damage (especially for spell that use the standard item card)  
added MinorQOL.forceRollDamage setting to assist macro writers. If true minor-qol will ignore the config setting and always roll damage.  
v1.0.42 pass damage total to dynmaiceffects so macros can use @damage as a parameter from minor-qol.  
v0.1.41 some fixes for better rolls damage type checking  
v0.1.40 added place template option to spell cast dialog.  
enabled spell template drawing even if no damage rolled.  
fixed introduced bug on timed out rolls  
improvements to self targeted spells/items.  
Fixed alt damage strings for most languages  
v0.1.38 Fix for item cards being show too often and fix for item cards not being shown in some cases.  
Support for next version of LMRTFY to have finer control of saving throws being assigned to spells waiting for player saves  
v0.1.36 fix for better-rolls area affect spells and minor-qol  
correct file name for portugese (brazil)
v0.1.35 Check and warning if cozy-player is disabling minor-qol  
fix for saving throws with no speed rolls  
fix for damage button display  
fix for rolls not displaying with dice-so-nice (maybe)  
changed speed rolls to global setting  
v0.1.34 updated language translations  
v0.1.33 bugfix for self targeted spells wen no targets selected -> applies effects to self  
bugfix if showing roll damage on attack card - correctly use versatile if oririnal request was for verstatile attack  
bugfix replicate dnd5e behaviour when healing and tempmax !== 0. Will heal to hp.max + hp.tempmax  
v0.1.31 Breaking Change for those writing doTrapMacros and relying on Hooks.("MinorQoLDamageApplied", this hook has changed to "MinorQolRollComplete" and it is called whenever the roll sequence is complete, i.e. an attack is made and misses, an attack is made, hits and the damage applied, damage rolled and saving throws made.  
v0.1.29 support for dice so nice - minor-qol messages delayed until roll is complete. Exception is damage applied chat message to DM so they can evaluate straight away.  
support for new config item - pre item speed roll/macro roll checks.  
You can now add any item specific checks you want before the roll is made. By default ships with 2 checks  
1) Item user is not incapacitated - which means has at least one HP.  
2) Check range for item usage. If the item specifies a target of ally, creature, enemey each of the targeted tokens is checked to make sure it is within range to the item user or the item roll is rejected. Also makes sure user has a token selected.  
To add your own item checks you need to do  
  Hooks.on("MinorQolPreItemRoll", yourHandler(actor, item, event) => {.......})  
  the actor is the actor performing the roll, item is the item being used and event is the ctl/shift/alt status of the roll  
  return false to stop the roll, true to allow it to proceed.  
v0.1.28 fixed double checking of consumption.
v0.1.27 added consumable deletion on last item use.  
fix for damage application when more 1 GM logged in. Should fix people having problems with traps  
added Hooks MinorQolDamageApplied so that traps can know when to restore targets.  
v0.1.26 Added ability to display item cards when doing speed rolls.  
The item card is displayed based on the new speedItemRolls settings of , Off|On|On + ItemCards. The module will display the actionbuttons on the card for actions that are not speed rolled.  
CLeaned up another edgee case for consumable consumption  
v0.1.25 Cleaned up resource/charge consumption in speed iteem rolls.  
v0.1.24 Fixed processing betterrolls saving throws when players do their own saves and LMRTFY not installed.  
cleaned up timeout handling for player saves  
support right click on charater sheet to do versatile attack  
corrected error on critical threshold in better rolls  
Updated Readme.md to be more current.  
v0.1.23 Correct application of effects for better rolls  
v0.1.22 support itemId in minorQOL.doRoll() as well as item name.  
v0.1.21 Added "no damage on save" text when parsing description for spells/items that do no damage on save.  
v0.1.20 fixed better rolls support:  
correctly determine hit/miss/critical on single/dual rolls with normal/advantage/disadvantage.  
v0.1.18 Fix many edge cases for player rolled saving throws for measured templates that target self.  
v0.1.17 Fix so that gmroll/blind roll is always respected.  
Fix to self targetting so preferentially self target, rather than self target when no other targets selected.  
v0.1.14 add AttackRollFlavorTextAlt, DamageRollFlavorTextAll, SavingThrowTextAlt so that you can have localized and non-localized versions of those strings if required.  
v0.1.13 1/2 damage on saves can now be configured to require the text "half as much dmage" or "half damage" to appear in the description as well as specifying a saving throw. Congure from module settings damage immunities.  
You can now use MESS templates for placing templates with minor-qol. The display will be slightly strange as MESS tempaltes targets all tokens during placement. minor-qol will then filter the targeted tokens on placement.  
In conjunction with dynamiceffects a new flag flags.dnd5e.attackAdvantage is supported for auto rolled attacks. -1 = disadvantage +1 = advantage. Applies to all rolled attacks.  
In conjunction with dynamiceffects a new flag forceCritical for auto rolled damage rolls. True => make any hit a critical.  
Minor-qol roll-damage button no longer keeps focus.  
Reverse damage cards now display all dr/di/dv fields to help the dm confirm the applied damage is correct.  
v0.1.12 Fixed behaviour of autoSaves on autoRoll damage off when roll damage button is pressed.  
Display spell cast dialog when casting via "standard roll button"  
fix for innacte spell casting consumption when charges = 1  
added option to always display saving throws.  
update ja.jason  
v0.1.07 fix for not parsing damage rolls correctly  
v0.1.05 +added untarget feature (module config). At the end of your turn untarget none, dead or all tokens. Requested a few times.  
added support for tidy5eSheet  
minor-qol now uses the default dnd5e spell cast dialog so you can choose not to consume a slot if you wish.  
added support for better rolls. This is an alpha release so there will be bugs.  
To use minor-qol with better rolls you should disable "speed rolls" and "add item sheet buttons" in minor-qol.  
You will need to set better rolls to only roll a single dice for attack rolls or it will break.  
All of the other minor-qol flags flags are supposed to work, auto-check hits, saves, apply damage etc.  
Damage immunities/resitance should work, but do check.  
Auto application of dynamiceffects effects is supported (disable via the minor-qol flag if you don't want that).  
When you find bugs, please try and give as much information as you can to help me work out what is going on.   
v 0.1.03 hopefully finally fixed the consumption to match the dnd5e standard  
0.1.01 v0.9 DND5e compatibility. Chages to item charge usage/consumption in line with new dnd 0.9 behaviour. This version REQUIRES DND 0.9   
0.1.00 fixed self targeting so that actions that have a target of self preferntially apply to self over the selected targets  
fixed damage/healing application when consuming the last potion of a given type.  
added advantage display for saving throws made with magic resistance.  
Change kr.json to ko.json  
0.96 Fix for concentration and saving throws for CUB  
0.95 Another go at hiding the above chat cards from the user.  
Changed autoRollDamage to allow always rolling damage even if attack misses.  
<BREAKING> Chnaged chatcard damage buttons so that apply damage ALWAYS reduces hit points, apply healing ALWAYS increases hit points.  
0.94 Changed autoCheckHit/autoCheckSave option to allow showing the chat card only to GM.  
0.93 fix for cantrips not working  
0.92 Chat damage buttons not working  
Casting spells with auto-target off causes the spell to not complete.  
0.90 fix for spell dc override, spells finally use slots correctly, template effects with a range of self do not target the caster.  
0.89 fixed typo in above fix.  
0.88 added compatibility for new spell slot features including pact magic.  
0.0.86 Fixed spell cast dialog to support pact spells  
Fixed problem with charge consumption for feats. 
0.0.85 add option to apply damage but don't display undo damage card.  
0.0.84 reinstate support for roll buttons on betternpcsheet.  
0.0.83 add support for DNDBeyondSheet5e for speed item rolls.  
0.0.82 fix bug with item buttons being added multiple times.  
If auto roll damage is disabled and speed item roll is enabled a roll damage button is added to the attack roll card.  